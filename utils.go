package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
)

func SendResponse(c *gin.Context, objectToRespond interface{}, code int) {
	bodyBytes, _ := json.Marshal(objectToRespond)
	c.Header("Content-Length", strconv.Itoa(len(bodyBytes)))
	c.Data(code, JsonByteStreamHeader, bodyBytes)
}

type ErrorResponse struct {
	Error string
}

func ReturnFailure(c *gin.Context, code int, err error) {
	errToReturn := new(ErrorResponse)
	errToReturn.Error = err.Error()
	sendBytes, _ := json.Marshal(errToReturn)
	c.Header("Content-Length", strconv.Itoa(len(sendBytes)))
	c.Data(code, JsonByteStreamHeader, sendBytes)
}

func getReqeust(uri string, toDecode interface{}) (interface{}, error) {
	resp, formErr := http.Get(uri)

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Encountered bad status code from service")
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	json.Unmarshal(buf.Bytes(), &toDecode)

	return toDecode, nil
}

func getReqeustAllow404(uri string, toDecode interface{}) (interface{}, error) {
	resp, formErr := http.Get(uri)

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 && resp.StatusCode != 404 {
		return nil, errors.New("Encountered bad status code from service")
	}

	if resp.StatusCode == 404 {
		return nil, nil
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	json.Unmarshal(buf.Bytes(), &toDecode)

	return toDecode, nil
}
