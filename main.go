package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"github.com/golang/protobuf/ptypes/struct"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/genproto/googleapis/cloud/dialogflow/v2"
	"net/http"
	"strconv"
	"time"
)

const JsonByteStreamHeader = "application/json; charset=utf-8"
const doctorHost = "http://doctorService.ap-southeast-2.elasticbeanstalk.com"
const timeslotHost = "http://timeslotService.ap-southeast-2.elasticbeanstalk.com"

func main() {
	r := gin.Default()
	r.POST("/chat", chat)
	r.Run(":5000") // listen and serve
}

func chat(c *gin.Context) {
	var err error

	wr := dialogflow.WebhookRequest{}
	if err = jsonpb.Unmarshal(c.Request.Body, &wr); err != nil {
		logrus.WithError(err).Error("Couldn't Unmarshal request to jsonpb")
		c.Status(http.StatusBadRequest)
		return
	}

	var resp string
	var respErr error

	switch wr.GetQueryResult().Intent.DisplayName {
	case "GetDoctor":
		resp, respErr = GetDoctor(wr)
	case "MakeBooking":
		resp, respErr = CheckBooking(wr)
	case "MakeBooking - yes":
		resp, respErr = ConfirmBooking(wr)
	case "GetDoctors":
		resp, respErr = GetDoctors(wr)
	case "GetBooking":
		resp, respErr = GetBooking(wr)
	case "CancelBooking":
		resp, respErr = WarningCancelBooking(wr)
	case "CancelBooking - yes":
		resp, respErr = CancelBooking(wr)
	case "GetAvailibleTimeslots":
		resp, respErr = GetAvailibleTimeslots(wr)
	default:
		resp = ""
		respErr = errors.New("Intent failed to match")
	}

	if respErr != nil {
		ReturnFailure(c, 500, respErr)
		fmt.Println(respErr)
		return
	}

	toReturn := new(dialogflow.WebhookResponse)

	toReturn.FulfillmentText = resp

	SendResponse(c, toReturn, 200)
}

func GetDoctor(wr dialogflow.WebhookRequest) (string, error) {
	firstName := wr.GetQueryResult().GetParameters().GetFields()["FirstName"].GetStringValue()
	lastName := wr.GetQueryResult().GetParameters().GetFields()["LastName"].GetStringValue()

	var toReturnDoctor *Doctor
	if firstName != "" && lastName != "" {
		toReturnDoctor = getDoctorByName(fmt.Sprintf("%s %s", firstName, lastName))
	} else {
		toReturnDoctor = getDoctorByName(firstName)
	}

	if toReturnDoctor == nil {
		return "Sorry, we don't have any doctors with that name", nil
	}

	messageToReturn := fmt.Sprintf("%s is a Doctor specialising in %s, who is located at %s. Their doctor ID is %d", toReturnDoctor.FullName, toReturnDoctor.Specialisation, toReturnDoctor.Location, toReturnDoctor.ID)

	return messageToReturn, nil
}

func CheckBooking(wr dialogflow.WebhookRequest) (string, error) {
	// take the time, etc and make it athing
	startTime := getTimeFromField(wr.GetQueryResult().Parameters.GetFields()["StartTime"])

	interval := wr.GetQueryResult().Parameters.GetFields()["Length"].GetStructValue()

	duration := getDurationFromInterval(interval)

	if duration == nil {
		return "Sorry but that's not a valid booking duration, bookings must be divisible by 30 minutes", nil
	}

	endTime := startTime.Add(*duration)

	doctorName := wr.GetQueryResult().GetParameters().GetFields()["Doctor"].GetStringValue()
	doctor := getDoctorByName(doctorName)

	if doctor == nil {
		return "Sorry we don't have a doctor with that name", nil
	}

	timeslots, timeErr := GetTimeslots(startTime, &endTime, doctor.ID, int64(duration.Minutes()))

	if timeErr != nil {
		return "", timeErr
	}

	idString := strconv.FormatInt(doctor.ID, 10)
	if len(timeslots[idString]) < 1 {
		return "Sorry that timeslot for that doctor isn't availible", nil
	}

	response := fmt.Sprintf("Ok, so would you like to confirm the booking at %s, for %d minutes with Doctor %s?", startTime.String(), int64(duration.Minutes()), doctor.FullName)

	return response, nil
}

func ConfirmBooking(wr dialogflow.WebhookRequest) (string, error) {
	toSendInfo := new(BookingInfo)
	toSendTimeslot := new(Timeslot)

	startTime := getTimeFromField(wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["StartTime"])

	interval := wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["Length"].GetStructValue()

	duration := getDurationFromInterval(interval)

	if duration == nil {
		return "Sorry but that's not a valid booking duration, bookings must be divisible by 30 minutes", nil
	}

	fmt.Println(duration.Minutes())
	endTime := startTime.Add(*duration)

	doctorName := wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["Doctor"].GetStringValue()

	doctor := getDoctorByName(doctorName)

	if doctor == nil {
		return "Sorry we don't have a doctor with that name", nil
	}

	timeslots, timeErr := GetTimeslots(startTime, &endTime, doctor.ID, int64(duration.Minutes()))

	if timeErr != nil {
		return "", timeErr
	}

	idString := strconv.FormatInt(doctor.ID, 10)
	if len(timeslots[idString]) < 1 {
		return "Sorry that timeslot for that doctor isn't availible", nil
	}

	toSendInfo.Location = wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["Location"].GetStringValue()
	toSendInfo.Reason = wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["Reason"].GetStringValue()
	toSendInfo.ClientName = wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["ClientName"].GetStringValue()
	toSendTimeslot.DoctorID = doctor.ID
	toSendTimeslot.StartTime = startTime.Unix()
	toSendTimeslot.EndTime = endTime.Unix()

	res, makeErr := MakeBooking(*toSendInfo, *toSendTimeslot)

	if makeErr != nil {
		return "", makeErr
	}

	resString := fmt.Sprintf("Ok, you booking for %s has been made. It's ID is %d", startTime.String(), res.ID)

	//now take each element and actually send the booking through
	return resString, nil
}

func GetDoctors(wr dialogflow.WebhookRequest) (string, error) {
	doctors, err := GetDoctorsAPI()

	if err != nil {
		return "", err
	}

	stringToReturn := "Our doctors include: "

	for _, val := range doctors {
		stringToReturn = fmt.Sprintf("%s Dr %s (%d)", stringToReturn, val.FullName, val.ID)
	}

	return stringToReturn, nil
}

func GetBooking(wr dialogflow.WebhookRequest) (string, error) {
	id := int64(wr.GetQueryResult().GetParameters().GetFields()["ID"].GetNumberValue())
	booking, err := GetBookingAPI(id)

	if err != nil {
		return "", err
	}

	if booking == nil {
		return "Booking not found", nil
	}

	startTime := time.Unix(booking.StartTime, 0)
	endTime := time.Unix(booking.EndTime, 0)

	doctor, doctorErr := GetDoctorAPI(booking.DoctorID)

	if doctorErr != nil {
		return "", doctorErr
	}

	if doctor == nil {
		return "Can't seem to find the doctor anymore", nil
	}

	toRespond := fmt.Sprintf("Booking %d : Starts at %s, Ends at %s, with Doctor %s at %s. Reason listed %s", booking.ID, startTime.String(), endTime.String(), doctor.FullName, booking.Location, booking.Reason)

	return toRespond, nil
}

func CancelBooking(wr dialogflow.WebhookRequest) (string, error) {
	id := int64(wr.GetQueryResult().GetOutputContexts()[0].GetParameters().GetFields()["ID"].GetNumberValue())

	err := CancelBookingAPI(id)

	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Booking %d has been cancelled", id), nil
}

func WarningCancelBooking(wr dialogflow.WebhookRequest) (string, error) {
	id := int64(wr.GetQueryResult().GetParameters().GetFields()["ID"].GetNumberValue())

	booking, _ := GetBookingAPI(id)

	if booking == nil {
		return fmt.Sprintf("Are you sure you want to cancel booking %d?", id), nil
	}

	startTime := time.Unix(booking.StartTime, 0)
	doctor, doctorErr := GetDoctorAPI(booking.DoctorID)

	if doctorErr != nil {
		return fmt.Sprintf("Are you sure you want to cancel booking %d?", id), nil
	}

	return fmt.Sprintf("Are you sure you want to cancel the booking with Dr %s at %s?", doctor.FullName, startTime.String()), nil
}

func GetAvailibleTimeslots(wr dialogflow.WebhookRequest) (string, error) {
	// get doctor

	doctorID := int64(0)
	interval := int64(0)

	startTime := getTimeFromField(wr.GetQueryResult().Parameters.GetFields()["StartTime"])
	endTime := getTimeFromField(wr.GetQueryResult().Parameters.GetFields()["EndTime"])

	nameField := wr.GetQueryResult().Parameters.GetFields()["Doctor"]
	doctor := getDoctorByName(nameField.GetStringValue())

	if doctor == nil && nameField != nil {
		return "Sorry we don't have a doctor by that name", nil
	} else if doctor != nil && nameField != nil {
		doctorID = doctor.ID
	}

	rawInterval := wr.GetQueryResult().Parameters.GetFields()["Length"].GetStructValue()

	duration := getDurationFromInterval(rawInterval)

	if duration == nil && rawInterval != nil {
		return "Sorry but that's not a valid booking duration, bookings must be divisible by 30 minutes", nil
	} else if duration != nil && rawInterval != nil {
		interval = int64(duration.Minutes())
	}

	timeslots, err := GetTimeslots(startTime, endTime, doctorID, interval)

	if err != nil {
		return "", err
	}

	if interval == 0 {
		interval = 30
	}

	if doctorID == 0 {
		// horrible
		for key, _ := range timeslots {
			doctorID, _ = strconv.ParseInt(key, 10, 64)
			break
		}
	}

	intermediary := fmt.Sprintf("Dr %s has %d minute bookings availible at", doctor.FullName, interval)
	extra := ""

	counter := 0
	for _, val := range timeslots[strconv.FormatInt(doctor.ID, 10)] {
		if counter > 3 {
			break
		}
		counter++
		time := time.Unix(val.StartTime, 0)
		extra = fmt.Sprintf("%s, %s", extra, time.String())
	}

	return fmt.Sprintf("%s%s", intermediary, extra), nil
}

func getTimeFromField(field *structpb.Value) *time.Time {

	iso := field.GetStringValue()

	if iso == "" {
		iso = field.GetStructValue().GetFields()["date_time"].GetStringValue()

		if iso == "" {
			return nil
		}
	}

	time, err := time.Parse(time.RFC3339, iso)

	if err != nil {
		panic(err)
	}

	return &time
}

func getDoctorByName(name string) *Doctor {
	found, foundErr := GetDoctorsByNameAPI(name)

	if foundErr != nil {
		panic(foundErr)
	}

	if len(found) < 1 {
		return nil
	}

	return found[0]
}

func getDurationFromInterval(interval *structpb.Struct) *time.Duration {
	rawLength := interval.GetFields()["amount"].GetNumberValue()
	unit := interval.GetFields()["unit"].GetStringValue()

	length := 0

	if unit == "min" {
		length += int(rawLength * 60)
	} else {
		length += int(rawLength * 60 * 60)
	}

	if length%(30*60) != 0 {
		return nil
	}

	duration, durationErr := time.ParseDuration(fmt.Sprintf("%ds", length))

	if durationErr != nil {
		panic(durationErr)
	}

	return &duration
}
