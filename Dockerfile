FROM golang:1.12

WORKDIR .

COPY . .

RUN go get github.com/gin-gonic/gin
RUN go get github.com/swaggo/gin-swagger
RUN go get github.com/swaggo/gin-swagger/swaggerFiles
RUN go get github.com/alecthomas/template
RUN go get github.com/swaggo/swag
RUN go get google.golang.org/genproto/googleapis/cloud/dialogflow/v2
RUN go get github.com/golang/protobuf/jsonpb
RUN go get github.com/sirupsen/logrus

ENV TZ=Australia/Sydney
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN go build -o chatbotService

EXPOSE 5000
CMD ["./chatbotService"]
