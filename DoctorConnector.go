package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type DoctorListResp struct {
	Doctors []*Doctor
}

type Doctor struct {
	DoctorMeta
	DoctorInfo
}

type DoctorMeta struct {
	ID        int64
	CreatedAt int64
}

type DoctorInfo struct {
	FullName       string
	Location       string
	Specialisation string
}

func GetDoctorsAPI() ([]*Doctor, error) {
	resp, error := getReqeust(fmt.Sprintf("%s/doctor", doctorHost), new(DoctorListResp))

	if resp != nil {
		return resp.(*DoctorListResp).Doctors, error
	}

	return nil, nil
}

func GetDoctorsByNameAPI(name string) ([]*Doctor, error) {
	toReq := fmt.Sprintf("%s?name=%s", fmt.Sprintf("%s/doctor", doctorHost), url.QueryEscape(name))
	resp, error := getReqeust(toReq, new(DoctorListResp))

	return resp.(*DoctorListResp).Doctors, error
}

func GetDoctorAPI(ID int64) (*Doctor, error) {
	uri := fmt.Sprintf("%s/doctor/%d", doctorHost, ID)
	resp, formErr := http.Get(uri)

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Encountered bad status code from Doctor service")
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	respParse := new(Doctor)

	json.Unmarshal(buf.Bytes(), &respParse)

	return respParse, nil
}
