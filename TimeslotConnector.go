package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type Booking struct {
	BookingMeta
	BookingInfo
	Timeslot
}

type BookingMeta struct {
	ID        int64
	CreatedAt int64
}

type BookingInfo struct {
	ClientName string
	Location   string
	Reason     string
}

type Timeslot struct {
	DoctorID  int64
	StartTime int64
	EndTime   int64
}

func GetTimeslots(startTime *time.Time, endTime *time.Time, doctorID int64, interval int64) (map[string][]*Timeslot, error) {
	queryString := ""
	if startTime != nil {
		queryString = fmt.Sprintf("%sstartTime=%d&", queryString, startTime.Unix())
	}

	if endTime != nil {
		queryString = fmt.Sprintf("%sendTime=%d&", queryString, endTime.Unix())
	}

	if doctorID != 0 {
		queryString = fmt.Sprintf("%sdoctorID=%d&", queryString, doctorID)
	}

	if interval != 0 {
		queryString = fmt.Sprintf("%sinterval=%d", queryString, interval)
	}

	var toGet map[string][]*Timeslot
	uri := fmt.Sprintf("%s/timeslot?%s", timeslotHost, queryString)
	resp, formErr := http.Get(uri)

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Encountered bad status code from Doctor service")
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	json.Unmarshal(buf.Bytes(), &toGet)

	return toGet, nil
}

type PostBookingRequest struct {
	BookingInfo `json:"BookingInfo"`
	Timeslot    `json:"Timeslot"`
}

func MakeBooking(bookingInfo BookingInfo, timeslot Timeslot) (*Booking, error) {
	toSend := new(PostBookingRequest)
	toSend.BookingInfo = bookingInfo
	toSend.Timeslot = timeslot
	booking := new(Booking)
	toBeSentBytes, _ := json.Marshal(toSend)

	uri := fmt.Sprintf("%s/booking", timeslotHost)

	resp, formErr := http.Post(uri, JsonByteStreamHeader, bytes.NewBuffer(toBeSentBytes))

	if formErr != nil {
		return nil, formErr
	}

	if resp.StatusCode != 200 {
		return nil, errors.New("Encountered bad status code from Timeslot service")
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	buf := bytes.NewBuffer(body)

	json.Unmarshal(buf.Bytes(), &booking)

	return booking, nil
}

func GetBookingAPI(ID int64) (*Booking, error) {
	booking := new(Booking)
	resp, err := getReqeustAllow404(fmt.Sprintf("%s/booking/%d", timeslotHost, ID), booking)

	if resp != nil {
		return resp.(*Booking), err
	}

	return nil, nil
}

func CancelBookingAPI(ID int64) error {
	uri := fmt.Sprintf("%s/booking/%d", timeslotHost, ID)

	req, err := http.NewRequest(http.MethodDelete, uri, nil)

	if err != nil {
		return err
	}

	resp, respErr := http.DefaultClient.Do(req)

	if respErr != nil {
		return respErr
	}

	if resp.StatusCode != 200 {
		return errors.New("Bad status code when deleting booking")
	}

	return nil
}
